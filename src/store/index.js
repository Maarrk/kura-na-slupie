import Vue from 'vue'
import Vuex from 'vuex'
import Clipboard from 'v-clipboard'
import * as crossing from '@/store/modules/crossing.js'
import * as config from '@/store/modules/config.js'

Vue.use(Vuex)
Vue.use(Clipboard)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    crossing,
    config,
  },
})
