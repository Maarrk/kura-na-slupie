export const state = {
  crossings: [],
  defaults: {},
}

let nextId = 1
export const mutations = {
  ADD_CROSSING(state, crossing) {
    state.crossings.push({
      id: nextId++,
      ...crossing,
    })
  },
  REMOVE_CROSSING(state, crossingToRemove) {
    state.crossings = state.crossings.filter(
      crossing => crossing.id !== crossingToRemove.id
    )
  },
  MODIFY_CROSSING(state, newCrossing) {
    let modifiedCrossing = state.crossings.find(
      crossing => crossing.id === newCrossing.id
    )
    Object.keys(newCrossing).forEach(prop => {
      modifiedCrossing[prop] = newCrossing[prop]
    })
  },
  CLEAR_CROSSINGS(state) {
    state.crossings = []
  },
  SET_DEFAULTS(state, defaults) {
    state.defaults = defaults
  },
}

export const actions = {
  addCrossing({ commit }, crossing) {
    commit('ADD_CROSSING', crossing)
  },
  removeCrossing({ commit }, crossingToRemove) {
    commit('REMOVE_CROSSING', crossingToRemove)
  },
  clearCrossings({ commit }) {
    commit('CLEAR_CROSSINGS')
  },
}

export const getters = {
  getCrossingById: state => id => {
    return state.crossings.find(crossing => crossing.id === id)
  },
  getCrossingIndexById: state => id => {
    return state.crossings.findIndex(crossing => crossing.id === id)
  },
  getCrossingCount: state => {
    return state.crossings.length
  },
}
