const props = {
  startX: { default: 0, name: 'start x' },
  startY: { default: 0, name: 'start y' },
  sheetWidth: { default: 86.1, name: 'szerokość rysunku' },
  sheetHeight: { default: 52.5, name: 'wysokość rysunku' },
  sheetBottomPadding: { default: 14, name: 'dolny margines' },
  rowCount: { default: 10, name: 'liczba w rzędzie' },
}

export const state = {
  startX: props.startX.default,
  startY: props.startY.default,
  sheetWidth: props.sheetWidth.default,
  sheetHeight: props.sheetHeight.default,
  sheetBottomPadding: props.sheetBottomPadding.default,
  rowCount: props.rowCount.default,
}

export const mutations = {
  SET_PROPERTIES(state, newProps) {
    Object.keys(newProps).forEach(prop => {
      state[prop] = newProps[prop]
    })
  },
}

export const actions = {
  parseConfigCsv({ commit }, configCsv) {
    let names = configCsv.split(/\r?\n/)[0].split(';')
    let values = configCsv.split(/\r?\n/)[1].split(';')
    if (names.length != values.length) {
      throw new Error('liczba parametrów i wartości różni się')
    }

    let updatedProps = {}
    let foundAny = false

    for (let i = 0; i < names.length; i++) {
      const name = names[i].toLowerCase()
      if (name.length == 0) continue

      let prop = Object.keys(props).find(prop => props[prop].name == name)
      if (prop) {
        updatedProps[prop] = parseFloat(values[i].replace(/,/, '.'))
        foundAny = true
      } else {
        throw new Error(`brak parametru o nazwie ${name}`)
      }
    }

    if (foundAny) {
      commit('SET_PROPERTIES', updatedProps)
    }
  },
  getConfigCsv() {
    let names = []
    let values = []
    Object.keys(props).forEach(prop => {
      names.push(props[prop].name)
      values.push(state[prop])
    })
    return names.join(';') + '\n' + values.join(';').replace(/\./, ',') + '\n'
  },
}
