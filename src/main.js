import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './styles/main.scss'

Vue.config.productionTip = false

Vue.filter('dimtext', function(value, places = 1) {
  // format as dimension text
  if (value === null) return ''
  return value.toFixed(places).replace('.', ',') + 'm'
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
